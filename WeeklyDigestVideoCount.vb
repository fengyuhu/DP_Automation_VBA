Sub WeeklyDigestAuto()
'
' WeeklyDigestAuto 宏
'

'
    Range("A1:V3976").Select
    Range("R8").Activate
    Selection.AutoFilter
    ActiveSheet.Range("$A$1:$V$3976").AutoFilter Field:=10, Criteria1:="=23", _
        Operator:=xlOr, Criteria2:="=102"
    ActiveSheet.Range("$A$1:$V$3976").AutoFilter Field:=10, Criteria1:="102"
    ActiveSheet.Range("$A$1:$V$3976").AutoFilter Field:=14, Criteria1:="1"
    Selection.Copy
    Sheets.Add After:=ActiveSheet
    ActiveSheet.Paste
    Columns("A:B").Select
    Application.CutCopyMode = False
    Selection.Delete Shift:=xlToLeft
    Columns("B:M").Select
    Selection.Delete Shift:=xlToLeft
    Columns("C:H").Select
    Selection.Delete Shift:=xlToLeft
    Sheets("工作表1").Select
    Columns("B:D").Select
    Selection.Copy
    Sheets("工作表2").Select
    Columns("D:D").Select
    ActiveSheet.Paste
    Columns("A:A").Select
    Application.CutCopyMode = False
    Selection.Copy
    Range("H1").Select
    ActiveSheet.Paste
    Application.CutCopyMode = False
    ActiveSheet.Range("$H$1:$H$3360").RemoveDuplicates Columns:=1, Header:= _
        xlYes
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "total_view"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "average_view"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "num_Video"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "Primary"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "Secondary"
    Range("I2").Select
    ActiveCell.FormulaR1C1 = "=SUMIF(C[-8]:C[-7],RC[-1],C[-7])"
    Range("I2").Select
    Selection.AutoFill Destination:=Range("I2:I570")
    Range("I2:I570").Select
    Range("J2").Select
    ActiveCell.FormulaR1C1 = "=AVERAGEIF(C[-9]:C[-8],RC[-2],C[-8])"
    Range("J2").Select
    Selection.AutoFill Destination:=Range("J2:J570")
    Range("J2:J570").Select
    Range("K2").Select
    ActiveCell.FormulaR1C1 = "=COUNTIF(C[-10],RC[-3])"
    Range("L2").Select
    ActiveCell.FormulaR1C1 = "=VLOOKUP(RC[-4],C[-8]:C[-6],2,FALSE)"
    Range("M2").Select
    ActiveCell.FormulaR1C1 = "=VLOOKUP(RC[-5],C[-9]:C[-7],3,FALSE)"
    Range("K2").Select
    Selection.AutoFill Destination:=Range("K2:K570")
    Range("K2:K570").Select
    Range("L2").Select
    Selection.AutoFill Destination:=Range("L2:L570")
    Range("L2:L570").Select
    Range("M2").Select
    Selection.AutoFill Destination:=Range("M2:M570")
    Range("M2:M570").Select
    Range("H1:M570").Select
    Range("I3").Activate
    Selection.AutoFilter
    ActiveSheet.Range("$H$1:$M$570").AutoFilter Field:=5, Criteria1:=Array( _
        "animal", "art", "beauty", "comedy", "daily snap", "dance", "figure shot", "fitness", _
        "food", "music", "sports", "travel", "untagged"), Operator:=xlFilterValues
    Selection.Copy
    Sheets.Add After:=ActiveSheet
    ActiveSheet.Paste
    Columns("E:E").Select
    Application.CutCopyMode = False
    Selection.Copy
    Columns("I:I").Select
    ActiveSheet.Paste
    Columns("F:F").Select
    Application.CutCopyMode = False
    Selection.Copy
    Columns("P:P").Select
    ActiveSheet.Paste
    Range("J1").Select
    Application.CutCopyMode = False
    ActiveCell.FormulaR1C1 = "totalview"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "numVideo"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "AverageView"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "Percentage"
    Range("Q1").Select
    Range("Q1").Select
    Range("J1:M1").Select
    Selection.Copy
    Range("Q1").Select
    ActiveSheet.Paste
    Columns("I:I").Select
    Application.CutCopyMode = False
    ActiveSheet.Range("$I$1:$I$430").RemoveDuplicates Columns:=1, Header:=xlNo
    Columns("P:P").Select
    ActiveSheet.Range("$P$1:$P$430").RemoveDuplicates Columns:=1, Header:=xlNo
    Range("J2").Select
    ActiveCell.FormulaR1C1 = "=SUMIF(C[-9]:C[-4],RC[-1],C[-8])"
    Range("J2").Select
    ActiveCell.FormulaR1C1 = "=SUMIF(C[-5],RC[-1],C[-8])"
    Range("J2").Select
    Selection.AutoFill Destination:=Range("J2:J14")
    Range("J2:J14").Select
    Range("K2").Select
    ActiveCell.FormulaR1C1 = "=SUMIF(C[-6],RC[-2],C[-7])"
    Range("K2").Select
    Selection.AutoFill Destination:=Range("K2:K14")
    Range("K2:K14").Select
    Range("L2").Select
    ActiveCell.FormulaR1C1 = "=RC[-2]/RC[-1]"
    Range("L2").Select
    Selection.AutoFill Destination:=Range("L2:L14")
    Range("L2:L14").Select
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "total"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "count"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "percentage"
    Range("M1:O1").Select
    Selection.Copy
    Range("T1").Select
    ActiveSheet.Paste
    Range("M2").Select
    Application.CutCopyMode = False
    ActiveCell.FormulaR1C1 = "=COUNTIF(C[-8],RC[-4])"
    Range("N2").Select
    ActiveCell.FormulaR1C1 = "=COUNT(C[-8])"
    Range("N2").Select
    Selection.ClearContents
    ActiveCell.FormulaR1C1 = "=COUNTIF(C[-8],"""")"
    Range("N2").Select
    ActiveCell.FormulaR1C1 = "=COUNTIF(C[-8],""<>"")"
    Range("O2").Select
    ActiveCell.FormulaR1C1 = "=RC[-2]/RC[-1]"
    Range("M2").Select
    Selection.AutoFill Destination:=Range("M2:M14"), Type:=xlFillDefault
    Range("M2:M14").Select
    Range("N2").Select
    Selection.AutoFill Destination:=Range("N2:N14"), Type:=xlFillDefault
    Range("N2:N14").Select
    Range("O2").Select
    Selection.AutoFill Destination:=Range("O2:O14"), Type:=xlFillDefault
    Range("O2:O14").Select
    Selection.Style = "Percent"
    Columns("M:N").Select
    Selection.EntireColumn.Hidden = True
    Range("Q2").Select
    ActiveCell.FormulaR1C1 = "=SUMIF(C[-16]:C[-11],RC[-1],C[-11])"
    Range("Q2").Select
    Selection.Cut
    Range("T2").Select
    ActiveSheet.Paste
    Range("U2").Select
    ActiveCell.FormulaR1C1 = "430"
    Selection.AutoFill Destination:=Range("U2:U56"), Type:=xlFillDefault
    Range("U2:U56").Select
    Range("T2").Select
    Selection.AutoFill Destination:=Range("T2:T56")
    Range("T2:T56").Select
    Range("T2").Select
    ActiveCell.FormulaR1C1 = "=CounSUMIF(C[-19]:C[-14],RC[-4],C[-14])"
    Range("T2").Select
    ActiveCell.FormulaR1C1 = "=COUNTIF(C[-14],RC[-4])"
    Range("T2").Select
    Selection.AutoFill Destination:=Range("T2:T56")
    Range("T2:T56").Select
    Range("V2").Select
    ActiveCell.FormulaR1C1 = "=RC[-2]/RC[-1]"
    Range("V2").Select
    Selection.AutoFill Destination:=Range("V2:V56")
    Range("V2:V56").Select
    Range("Q2").Select
    Columns("P:P").EntireColumn.AutoFit
    ActiveCell.FormulaR1C1 = "=SUMIF(C[-16]:C[-11],RC[-1],C[-15])"
    Range("Q2").Select
    Selection.AutoFill Destination:=Range("Q2:Q56")
    Range("Q2:Q56").Select
    Range("Q2").Select
    ActiveCell.FormulaR1C1 = "=SUMIF(C[-12],RC[-1],C[-15])"
    Range("Q2").Select
    Selection.AutoFill Destination:=Range("Q2:Q56")
    Range("Q2:Q56").Select
    Range("Q2").Select
    ActiveCell.FormulaR1C1 = "=SUMIF(C[-11],RC[-1],C[-15])"
    Range("Q2").Select
    Selection.AutoFill Destination:=Range("Q2:Q56")
    Range("Q2:Q56").Select
    Range("R2").Select
    ActiveCell.FormulaR1C1 = "=SUMIF(C[-12],RC[-2],C[-14])"
    Range("R2").Select
    Selection.AutoFill Destination:=Range("R2:R56")
    Range("R2:R56").Select
    Range("S2").Select
    ActiveCell.FormulaR1C1 = "=RC[-2]/RC[-1]"
    Range("S2").Select
    Selection.AutoFill Destination:=Range("S2:S56")
    Range("S2:S56").Select
    Columns("T:U").Select
    Selection.EntireColumn.Hidden = True
    Columns("V:V").Select
    Selection.Style = "Percent"
End Sub
